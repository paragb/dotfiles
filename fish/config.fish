# Path to your oh-my-fish.
set fish_path $HOME/.oh-my-fish

# Theme
set fish_theme krisleech

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-fish/plugins/*)
# Custom plugins may be added to ~/.oh-my-fish/custom/plugins/
set fish_plugins git-flow jump percol



function balias --argument alias command
  eval 'alias $alias $command'
  if expr match $command '^sudo '>/dev/null
    set command (expr substr + $command 6 (expr length $command))
  end
  complete -c $alias -xa "(
    set -l cmd (commandline -pc | sed -e 's/^ *\S\+ *//' );
    complete -C\"$command \$cmd\";
  )"
end
balias j 'jump'
balias efr 'emacsclient -c'
balias sefr 'sudo emacsclient -t --socket-name /tmp/nis/emacs1000/server'

set PATH $PATH /home/nis/bin /home/nis/coding/dart-sdk/bin

function fish_user_key_bindings
  bind \cr percol_select_history
end

echo '                   '(set_color F00)'___
    ___======____='(set_color 268BD2)'-'(set_color FF0)'-'(set_color 268BD2)'-='(set_color F00)')
  /T            \_'(set_color FF0)'--='(set_color 268BD2)'=='(set_color F00)')
  [ \ '(set_color 268BD2)'('(set_color FF0)'0'(set_color 268BD2)')   '(set_color F00)'\~    \_'(set_color FF0)'-='(set_color 268BD2)'='(set_color F00)')
   \      / )J'(set_color 268BD2)'~~    \\'(set_color FF0)'-='(set_color F00)')
    \\\\___/  )JJ'(set_color 268BD2)'~'(set_color FF0)'~~   '(set_color F00)'\)
     \_____/JJJ'(set_color 268BD2)'~~'(set_color FF0)'~~    '(set_color F00)'\\
     '(set_color 268BD2)'/ '(set_color FF0)'\  '(set_color FF0)', \\'(set_color F00)'J'(set_color 268BD2)'~~~'(set_color FF0)'~~     '(set_color 268BD2)'\\
    (-'(set_color FF0)'\)'(set_color F00)'\='(set_color 268BD2)'|'(set_color FF0)'\\\\\\'(set_color 268BD2)'~~'(set_color FF0)'~~       '(set_color 268BD2)'L_'(set_color FF0)'_
    '(set_color 268BD2)'('(set_color F00)'\\'(set_color 268BD2)'\\)  ('(set_color FF0)'\\'(set_color 268BD2)'\\\)'(set_color F00)'_           '(set_color FF0)'\=='(set_color 268BD2)'__
     '(set_color F00)'\V    '(set_color 268BD2)'\\\\'(set_color F00)'\) =='(set_color 268BD2)'=_____   '(set_color FF0)'\\\\\\\\'(set_color 268BD2)'\\\\
            '(set_color F00)'\V)     \_) '(set_color 268BD2)'\\\\'(set_color FF0)'\\\\JJ\\'(set_color 268BD2)'J\)
                        '(set_color F00)'/'(set_color 268BD2)'J'(set_color FF0)'\\'(set_color 268BD2)'J'(set_color F00)'T\\'(set_color 268BD2)'JJJ'(set_color F00)'J)
                        (J'(set_color 268BD2)'JJ'(set_color F00)'| \UUU)
                         (UU)'(set_color normal)
echo
echo

# Path to your custom folder (default path is $FISH/custom)
#set fish_custom $HOME/dotfiles/oh-my-fish

# Load oh-my-fish configuration.
. $fish_path/oh-my-fish.fish
