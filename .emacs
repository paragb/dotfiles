(require 'package)
(package-initialize)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/")
              '("elpa" . "http://jorgenschaefer.github.io/packages/") )










(require 'smooth-scrolling)
(require 'ace-jump-mode)
(require 'popwin)
(require 'rainbow-mode)
(require 'tramp)





(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)



(add-to-list 'load-path "~/.emacs.d/verilog/")
(autoload 'verilog-mode "verilog-mode" "Verilog mode" t )
(add-to-list 'auto-mode-alist '("\\.[ds]?vh?\\'" . verilog-mode))




(add-to-list 'auto-mode-alist '("\\.*rc$" . conf-unix-mode))


;(add-to-list 'company-backends 'company-c-headers)


  





(undo-tree-mode)
(menu-bar-mode -1)
(load-theme 'solarized-dark t)
(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "google-chrome-beta")
(popwin-mode 1)
(setq-default left-margin-width 0 right-margin-width 0)
(global-ace-isearch-mode)
(setq helm-fuzzy-match t)
(setq helm-M-x-fuzzy-match t)
(setq save-interprogram-paste-before-kill t)
(scroll-bar-mode -1)




;;popwin configuratiion
(setq display-buffer-function 'popwin:display-buffer)
(push '(term-mode :position :bottom :height 16 :stick t) popwin:special-display-config)
(push '("^\*Helm Swoop*$" :position :bottom :height 16 :stick t) popwin:special-display-config)
(push "*Kill Ring*" popwin:special-display-config)
(push '("^\*Helm .+\*$" :position :bottom :height 16 :regexp t) popwin:special-display-config)
(push '("^\*helm-.+\*$" :stick t :position :bottom :height 16 :regexp t) popwin:special-display-config)





(global-set-key (kbd "C-/") 'undo-tree-undo)
(global-set-key (kbd "C-?") 'undo-tree-undo)
(global-set-key (kbd "C-c SPC") 'ace-jump-word-mode)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(define-key global-map (kbd "C-c r") 'vr/replace)
(define-key global-map (kbd "C-q") 'vr/query-replace)
(global-set-key (kbd "C-M-s") 'flx-isearch-forward)
(global-set-key (kbd "C-M-r") 'flx-isearch-backward)
(global-set-key (kbd "C-M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x C-c") 'delete-frame)
(global-set-key (kbd "C->") 'end-of-buffer)
(global-set-key (kbd "C-<") 'beginning-of-buffer)
(global-set-key (kbd "M-n") 'forward-paragraph)
(global-set-key (kbd "M-p") 'backward-paragraph)
(global-set-key (kbd "C-x C-m") 'helm-M-x)
(global-set-key (kbd "C-c C-m") 'helm-M-x)
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)
















(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("6a37be365d1d95fad2f4d185e51928c789ef7a4ccf17e7ca13ad63a8bf5b922f" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "bfa3d52c7e3bbf528760bdbb8b59a69beda8d8b257d60a1b3ac26c1e5bc190bb" default)))
 '(graphene-default-font "Source Code Pro Semi Bold-10")
 '(graphene-fixed-pitch-font "Source Code Pro Semi Bold-10")
 '(graphene-variable-pitch-font "Source Code Pro Semi Bold-10")
 '(magit-use-overlays nil)
 '(menu-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(transient-mark-mode (quote (only . t))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Pro" :foundry "adobe" :slant normal :weight semi-bold :height 110 :width normal))))
 '(flymake-errline ((t (:underline nil))))
 '(flymake-warnline ((t (:underline nil))))
 '(mode-line ((t (:background "black" :foreground "gray60" :inverse-video nil :box (:line-width 1 :color "#073642" :style unspecified) :overline "#073642" :underline "black"))))
 '(mode-line-inactive ((t (:background "#155546" :foreground "gray60" :inverse-video nil :box nil :overline nil :underline nil)))))
(sml/setup)

(require 'auto-complete-auctex)
(defun my:ac-c-headers-init ()
  (require 'auto-complete-c-headers)
  (add-to-list 'ac-sources 'ac-source-c-headers))

(add-hook 'c++-mode-hook 'my:ac-c-headers-init)
(add-hook 'c-mode-hook 'my:ac-c-headers-init)

(print "the")

(require 'auto-complete-clang)
;(setq ac-auto-start nil)
;(setq ac-quick-help-delay 0.5)
; (ac-set-trigger-key "TAB")
;; (define-key ac-mode-map  [(control tab)] 'auto-complete)
;(define-key ac-mode-map  (kbd "M-TAB") 'auto-complete)
(defun my-ac-config ()
  (setq-default ac-sources '(ac-source-abbrev ac-source-dictionary ac-source-words-in-same-mode-buffers))
  (add-hook 'emacs-lisp-mode-hook 'ac-emacs-lisp-mode-setup)
  ;; (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)
    (add-hook 'auto-complete-mode-hook 'ac-common-setup)
  (global-auto-complete-mode t))
(defun my-ac-cc-mode-setup ()
  (setq ac-sources (append '(ac-source-clang) ac-sources)))
(add-hook 'c-mode-common-hook 'my-ac-cc-mode-setup)
;; ac-source-gtags
(my-ac-config)

(setq ac-clang-flags
      (mapcar (lambda (item)(concat "-I" item))
              (split-string
               "
 /usr/lib/gcc/x86_64-pc-linux-gnu/4.8.3/include/g++-v4
 /usr/lib/gcc/x86_64-pc-linux-gnu/4.8.3/include/g++-v4/x86_64-pc-linux-gnu
 /usr/lib/gcc/x86_64-pc-linux-gnu/4.8.3/include/g++-v4/backward
 /usr/lib/gcc/x86_64-pc-linux-gnu/4.8.3/include
 /usr/lib/gcc/x86_64-pc-linux-gnu/4.8.3/include-fixed
 /usr/include
"
               )))
