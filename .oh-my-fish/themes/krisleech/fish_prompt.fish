# name: Krisleech
function _git_branch_name
  echo (command git symbolic-ref HEAD ^/dev/null | sed -e 's|^refs/heads/||')
end

function _is_git_dirty
  echo (command git status -s --ignore-submodules=dirty ^/dev/null)
end

function fish_prompt
  set -l cyan (set_color 2AA198)
  set -l yellow (set_color 00ffff)
  set -l red (set_color ff0000)
  set -l blue (set_color 268BD2)
  set -l green (set_color 859900)
  set -l magenta (set_color D33682)
  set -l normal (set_color normal)
  set -l white (set_color white)
  set -l cwd (basename (prompt_pwd))

  if [ (_git_branch_name) ]
    set -l git_branch (_git_branch_name)
    set git_info "$white$git_branch "

    if [ (_is_git_dirty) ]
      set -l dirty "$yellow✗"
      set git_info "$git_info$dirty"
    end
  end

  echo -n -s $green $cwd $red '|' $git_info $yellow '❯' $normal
end
