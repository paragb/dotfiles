#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

export PATH="/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/opt/bin:/usr/x86_64-pc-linux-gnu/gcc-bin/4.8.3:/home/nis/bin:/home/nis/coding/dart-sdk/bin"



alias efr="emacsclient -c"
alias sefr="sudo emacsclient -t --socket-name /tmp/nis/emacs1000/server"




# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

autoload -Uz promptinit
promptinit
prompt purity

# Customize to your needs...
# Customize to your needs...
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Setup zsh-autosuggestions
source /home/nis/.zsh-autosuggestions/autosuggestions.zsh

# # Enable autosuggestions automatically
# zle-line-init() {
#     zle autosuggest-start
# }

# zle -N zle-line-init

# # use ctrl+t to toggle autosuggestions(hopefully this wont be needed as
# # zsh-autosuggestions is designed to be unobtrusive)
# bindkey '^T' autosuggest-toggle
