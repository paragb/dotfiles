  ace-isearch        20150105.... installed             A seamless bridge between isearch and ace-jump-mode
  ace-jump-mode      20140616.115 installed             a quick cursor location minor mode for emacs
  ace-jump-zap       20141208.926 installed             Character zapping, `ace-jump-mode` style
  async              20141229.... installed             Asynchronous processing in Emacs
  auctex             11.88        installed             Integrated environment for *TeX*
  auctex-latexmk     20140904.... installed             Add LatexMk support to AUCTeX
  auto-complete      20141228.633 installed             Auto Completion for GNU Emacs
  auto-complete-a... 20140223.958 installed             auto-completion for auctex
  auto-complete-c... 20140325.835 installed             An auto-complete source for C/C++ header files
  auto-complete-c... 20140409.52  installed             Auto Completion source for clang for GNU Emacs
  company            20150109.... installed             Modular text completion framework
  company-auctex     20141111.533 installed             Company-mode auto-completion for AUCTeX
  company-c-headers  20140930.... installed             Company mode backend for C/C++ header files
  dart-mode          20140815.... installed             Major mode for editing Dart files
  dash               20141220.... installed             A modern list library for Emacs
  dired+             20150104.... installed             Extensions to Dired.
  elpy               20141031.945 installed             Emacs Python Development Environment
  epl                20140823.609 installed             Emacs Package Library
  exec-path-from-... 20141212.846 installed             Get environment variables such as $PATH from the shell
  find-file-in-pr... 20141214.... installed             Find files in a project quickly.
  flycheck           20150109.147 installed             Modern on-the-fly syntax checking for GNU Emacs
  git-commit-mode    20141014.... installed             Major mode for editing git commit messages
  git-rebase-mode    20140928.... installed             Major mode for editing git rebase files
  graphene           20141030.219 installed             Friendly Emacs defaults
  helm               20150111.122 installed             Helm is an Emacs incremental and narrowing framework
  helm-swoop         20141222.... installed             Efficiently hopping squeezed lines powered by helm interface
  highlight-inden... 20150109.... installed             Minor modes for highlighting indentation
  idomenu            20141123.... installed             imenu tag selection a la ido
  let-alist          1.0.3        installed             Easily let-bind values of an assoc-list by their names
  magit              20141228.... installed             control Git from Emacs
  pkg-info           20140610.630 installed             Information about packages
  popup              20141215.349 installed             Visual Popup User Interface
  popwin             20141227.... installed             Popup Window Manager.
  project-persist    20131030.... installed             A minor mode to allow loading and saving of project settings.
  pyvenv             20150110.421 installed             Python virtual environment interface
  rainbow-delimiters 20141221.925 installed             Highlight brackets according to their depth
  rainbow-mode       0.10         installed             Colorize color names in buffers
  rich-minority      20141225.820 installed             Clean-up and Beautify the list of minor-modes.
  smart-mode-line    20150106.... installed             A color coded smart mode-line.
  smartparens        20150109.... installed             Automatic insertion, wrapping and paredit-like navigation with user defined pairs.
  smex               20141210.... installed             M-x interface with Ido-style fuzzy matching.
  sml-modeline       20120110.... installed             Show position in a scrollbar like way in mode-line
  smooth-scroll      20130321.... installed             Minor mode for smooth scrolling and in-place scrolling.
  smooth-scrolling   20131219.... installed             Make emacs scroll smoothly
  solarized-theme    20141216.... installed             The Solarized color theme, ported to Emacs.
  sr-speedbar        20141004.532 installed             Same frame speedbar
  undo-tree          20140509.522 installed             Treat undo history as a tree
  visual-regexp      20140926.408 installed             A regexp/replace command for Emacs with interactive visual feedback
  web-mode           20150105.... installed             major mode for editing web templates
  yasnippet          20141223.303 installed             Yet another snippet extension for Emacs.
